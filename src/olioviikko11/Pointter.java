/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioviikko11;

import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.input.MouseEvent;
/**
 *
 * @author Jönnsson
 */
public class Pointter {
    Circle circle;
    Boolean clickForMore;
    Boolean clicked;

    public Circle getCircle() {
        return circle;
    }
    String name;
    
    public Pointter(Double x, Double y){
        circle = new Circle(x,y,10,Color.BLACK);
        name = "default";
        clickForMore = false;
        clicked = false;
        ShapeHandler s = new ShapeHandler(this);
        ArrayList<Pointter> shapes = s.getShapes();
        System.out.println("pisteitä kartalla: " + shapes.size());
        circle.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent e) {
                clickForMore = true;
                clicked = true;
            }
        });

        
    }

}
