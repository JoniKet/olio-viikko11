/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioviikko11;


import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Jönnsson
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane background;
 
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void addCircle(MouseEvent event) {
        boolean jatka = true;
        if(ShapeHandler.getShapes().isEmpty() == false){
            for(Pointter pointter:ShapeHandler.getShapes()){
                if(pointter.clickForMore == true){
                    jatka = false;
                    pointter.clickForMore = false;
                }
            }
        }    
        if(jatka == true){
            Pointter p = new Pointter(event.getX(),event.getY());
            background.getChildren().add(p.getCircle());
        }
        addLine();
    }

   private void addLine() {
        ShapeHandler s = new ShapeHandler();
        if(s.getShapes().isEmpty() == false){
            for(Pointter pointter:s.getShapes()){
                if(pointter.clicked == true){
                    s.getLineCircles().add(pointter);
                    System.out.println("Pisteitä viivaan: " + s.getLineCircles().size());
                    pointter.clicked = false;
                }
            }
            if(ShapeHandler.shapes.get(ShapeHandler.shapes.size()-1).clicked == false && ShapeHandler.lineCircles.size() > 1){
                for(Pointter p1 :s.getLineCircles()) {
                    
                    Line line = new Line();
                    line.setStartX(p1.getCircle().getCenterX());
                    line.setStartY(p1.getCircle().getCenterY());
                    line.setEndX(s.getLineCircles().get(s.getLineCircles().indexOf(p1)+1).getCircle().getCenterX());
                    line.setEndY(s.getLineCircles().get(s.getLineCircles().indexOf(p1)+1).getCircle().getCenterY());
                    
                }
           }
       
        }
    
    }
}
