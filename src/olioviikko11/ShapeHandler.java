/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioviikko11;
import java.util.ArrayList;
/**
 *
 * @author Jönnsson
 */
public class ShapeHandler {
    
    static ArrayList<Pointter> shapes = new ArrayList<>();
    
    static ArrayList<Pointter> lineCircles = new ArrayList<>();

    public ArrayList<Pointter> getLineCircles() {
        return lineCircles;
    }

    public void setLineCircles(ArrayList<Pointter> lineCircles) {
        this.lineCircles = lineCircles;
    }

    public ShapeHandler(Pointter pointter){
        shapes.add(pointter);
    }
    
    public ShapeHandler(){
    }

    public static ArrayList<Pointter> getShapes() {
        return shapes;
    }

    
    
    
}
